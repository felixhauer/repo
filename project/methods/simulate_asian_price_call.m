function c_asian = simulate_asian_price_call(T_vector, r, K, X)
N = length(T_vector)-1;
c_asian = zeros(N,1);
T = T_vector(end);

for i = 1:N+1
    
    expected_win = max(mean(X(:,i))-K,0);
    c_asian(i) = exp(-r*T)*expected_win;

end

end