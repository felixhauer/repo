function c_asian = simulate_asian_strike_call(T_vector, r, X)
N = length(T_vector)-1;
c_asian = zeros(N,1);
T = T_vector(end);

for i = 1:N+1
    
    expected_win = max(X(end,i)-mean(X(:,i)),0);
    c_asian(i) = exp(-r*T)*expected_win;

end

end