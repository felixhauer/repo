function c = simulate_european_call(x, K, r, sigma, T_vector)

T = T_vector(end);
length_x = length(x);
c = zeros(length_x,1);
Z_c = box_mueller(length_x);

for i = 1:length_x
    
    arg_exp = (r- (sigma^2)/2)*(T-T_vector(i)) + Z_c(i)*sigma*sqrt(T-T_vector(i));
    arg = x(i) * exp(arg_exp) - K;
    c(i) = exp(-r*(T-T_vector(i))) * max((arg),0);

end

end

