function [x, T_vector] = simulate_x(x_start, r, sigma, N, T)

length_x = N+1;
Z_x = box_mueller(N);
T_delta = T/N;

x = zeros(length_x,1);
T_vector = zeros(length_x,1);

x(1) = x_start;
T_vector(1) = 0; 

for i = 1:N
   arg = (r- (sigma^2)/2)*(T_delta) + Z_x(i)*sigma*sqrt(T_delta);
   x(i+1) = exp(arg)*x(i);
   T_vector(i+1) = i*T_delta; 
   
end

end