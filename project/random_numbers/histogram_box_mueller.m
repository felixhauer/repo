clc 
clear


addpath(genpath('..\methods'))

seed = 0;
rng(seed);

N = 1e6; 

X_box_mueller = box_mueller(N);
density_normal_distribution = @(x) (1/sqrt(2*pi))*exp(-0.5*x.^2);



FigHandle = figure;
set(FigHandle, 'Position', [100, 100, 800, 500]);

hold on 
nbins = 100;
histogram(X_box_mueller,nbins,'Normalization','pdf')


x = -4.5:0.01:4.5;

plot(x, density_normal_distribution(x),'LineWidth',1.7)
xlabel('$$x$$','Interpreter','latex');

leg1 = legend('$v_i$','$\varphi(x)$');
set(leg1,'Interpreter','latex');
set(leg1,'FontSize',14);


box('on')

hold off

print(gcf,'-depsc', ['HistogramBoxMueller.eps']); 