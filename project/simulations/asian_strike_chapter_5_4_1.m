clc
clear

rng(1)

T = 1;
N = 100; 
T_vector = get_T_vector(T,N);

x_start = 100; 
r = 0.05;
sigma = 0.20; 

X = get_x_values(x_start, T_vector, r, sigma);

X_means = zeros(N+1,1);
for i = 1:N+1
   X_means(i) = mean(X(:,i)); 
end

X_endvalues = X(end,:);

c_asian = simulate_asian_strike_call(T_vector, r, X);


mean_c = mean(c_asian);


disp(['Endpreis Option:             ', num2str(c_asian(end))])
disp(['Endwert dursch. Aktienpreis  ', num2str( mean(X(:,end)))])
disp(['x(T) - x_bar(T):             ', num2str(X(end,end) - mean(X(:,end)))])
disp(['Durchschnittspreis Option:   ', num2str(mean_c)])

FigHandle = figure;
set(FigHandle, 'Position', [100, 100, 800, 500]);


ax1 = subplot(2,1,1);
plot(ax1, 1:N+1, X_means, 1:N+1, X_endvalues)
line([T_vector(1) N+1], [x_start x_start],'Color', 'green', 'LineStyle','--');
title(ax1,'$$\bf{Durchschnittlicher~Aktienpreis} $$','Interpreter','latex');
ylabel(ax1,'$$\bar{x(i)}$$','Interpreter','latex');
xlabel(ax1,'$$i$$','Interpreter','latex');
xlim(ax1, [0 N+1]);
leg1 = legend(ax1, '$\bar{x_i(t)}$', '$x_i(T)$',  '$x_i(t_0)$', 'Location','northwest');
set(leg1,'Interpreter','latex');
grid('on')

ax2 = subplot(2,1,2);
plot(ax2, T_vector, c_asian)
title(ax2,'$$\bf{Wertentwicklung~Asian~Strike~Call} $$','Interpreter','latex');
ylabel(ax2,'$$c(t)$$','Interpreter','latex');
xlabel(ax2,'$$t$$','Interpreter','latex');
grid('on')

print(gcf,'-depsc', ['AsianStrikeCall.eps']); 





function X = get_x_values(x_start, T_vector, r, sigma)
N = length(T_vector)-1; %correcting that x and T_vector have same length
T = T_vector(end);



X = zeros(N+1,N+1);

for i = 1:N+1
   X(:,i) =  simulate_x(x_start, r, sigma,N,T);
end
end


function T_vector = get_T_vector(T,N)

T_delta = T/N;
indices = 0:N;
T_vector = indices * T_delta;

end





