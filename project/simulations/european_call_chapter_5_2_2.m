clc
clear

addpath(genpath('..\methods'))

rng(50)

x_start = 100;
K = 95;
r = 0.05;
sigma_x = 0.20;
sigma_c = sigma_x;


T = 1;
N = 100; 


[x, T_vector] = simulate_x(x_start, r, sigma_x, N, T);

rng(0)
c_european_1 = simulate_european_call(x, K, r, sigma_c, T_vector);

rng(1)
c_european_2 = simulate_european_call(x, K, r, sigma_c, T_vector);

rng(2)
c_european_3 = simulate_european_call(x, K, r, sigma_c, T_vector);

mean(c_european_1)
mean(c_european_2)
mean(c_european_3)


sigma_c = 0.05;

rng(3)
c_european_4 = simulate_european_call(x, K, r, sigma_c, T_vector);

rng(4)
c_european_5 = simulate_european_call(x, K, r, sigma_c, T_vector);

rng(5)
c_european_6 = simulate_european_call(x, K, r, sigma_c, T_vector);

mean(c_european_4)
mean(c_european_5)
mean(c_european_6)


FigHandle = figure;
set(FigHandle, 'Position', [100, 100, 800, 760]);


ax1 = subplot(3,1,1);
plot(ax1, T_vector, x)
title(ax1,'$$\bf{Aktienpreis} $$','Interpreter','latex');
line([T_vector(1) T_vector(end)], [K K],'Color', 'red', 'LineStyle','--');
ylabel(ax1,'$$x(t)$$','Interpreter','latex');
xlabel(ax1,'$$t$$','Interpreter','latex');
legend(ax1, 'x(t)', 'K')
legend(ax1,'Location','northwest')
grid('on')

ax2 = subplot(3,1,2);
plot(ax2, T_vector, c_european_1,T_vector, c_european_2, T_vector, c_european_3)
title(ax2,'$$\bf{European~Call~} \sigma = 0.2  $$','Interpreter','latex');
ylabel(ax2,'$$c_i(t)$$','Interpreter','latex');
xlabel(ax2,'$$t$$','Interpreter','latex');
legend(ax2, 'c_1(t)', 'c_2(t)', 'c_3(t)')
legend(ax2,'Location','northwest')
ylim(ax2,[0 100])
grid('on')


ax3 = subplot(3,1,3);
plot(ax3, T_vector, c_european_4,T_vector, c_european_5, T_vector, c_european_6)
title(ax3,'$$\bf{European~Call~} \sigma = 0.05 $$','Interpreter','latex');
ylabel(ax3,'$$c_i(t)$$','Interpreter','latex');
xlabel(ax3','$$t$$','Interpreter','latex');
legend(ax3, 'c_4(t)', 'c_5(t)', 'c_6(t)')
legend(ax3,'Location','northwest')
ylim(ax3,[0 100])
grid('on')

print(gcf,'-depsc', ['EuropeanCallSeveral.eps']); 
