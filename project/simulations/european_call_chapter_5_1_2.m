clc
clear

addpath(genpath('..\methods'))

rng(13)


x_start = 100;
K = 95;
r = 0.15;
sigma_x = 0.20;
sigma_c = sigma_x;

T = 1;
N = 100; 


[x, T_vector] = simulate_x(x_start, r, sigma_x, N, T);
c_european = simulate_european_call(x, K, r, sigma_c, T_vector);

mean_c = mean(c_european);


disp(['Endpreis Aktie:              ', num2str(x(end))])
disp(['Endpreis Option:             ', num2str(c_european(end))])
disp(['x(T) - K:                    ', num2str(x(end) - K)])
disp(['Durchschnittspreis Option:   ', num2str(mean_c)])


FigHandle = figure;
set(FigHandle, 'Position', [100, 100, 800, 500]);

ax1 = subplot(2,1,1);
plot(ax1, T_vector, x)
title(ax1,'$$\bf{Aktienpreis} $$','Interpreter','latex');
line([T_vector(1) T_vector(end)], [K K],'Color', 'red', 'LineStyle','--');
ylabel(ax1,'$$x(t)$$','Interpreter','latex');
xlabel(ax1,'$$t$$','Interpreter','latex');
legend(ax1, 'x(t)', 'K')
legend(ax1,'Location','northwest')
grid('on')

ax2 = subplot(2,1,2);
plot(ax2, T_vector, c_european)
title(ax2,'$$\bf{Preis~European~Call} $$','Interpreter','latex');
ylabel(ax2,'$$c(t)$$','Interpreter','latex');
xlabel(ax2,'$$t$$','Interpreter','latex');
grid('on')


print(gcf,'-depsc', ['EuropeanCall1_higher_r.eps']); 






