clc
clear

t = clock;
seed = int32(t(6)*100);

n = 10000;
T_end = 100;
b_start = 0;

t_delta = T_end / n; 
rng(seed,'twister');
z = randn(n,1);

b = zeros(n,1);
b(1) = b_start;

for i = 2:n
b(i) = b(i-1) + z(i) * sqrt(t_delta);
end


x = 0:t_delta:T_end;
x = x(1:end-1);

plot(x,b)





