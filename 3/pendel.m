clc 
clear

N = 900000; 

phi = zeros(1,N);
omega = zeros(1,N); 

phi_0 = pi/4; 
omega_0 = pi/2; 
psi_0 = 0; 

phi(1) = phi_0;
omega(1) = omega_0; 


phi_current = phi_0; 
omega_current = omega_0; 
psi_current = psi_0; 


for i = 2:N
   
   [phi_next, omega_next, psi_next] = diff_eq(phi_current, omega_current, psi_current);
   
   phi(i) = phi_next; 
   omega(i) = omega_next; 
   
   phi_current = phi_next; 
   omega_current = omega_next; 
   psi_current = psi_next; 
   

   
end

phi

plot(phi,omega)




function [phi_next, omega_next, psi_next] = diff_eq(phi_current, omega_current, psi_current)

delta_t = 0.0001;
omega_N = 0.8; 
D = 0.7; 
A = 1.89; 


phi_next = phi_current + delta_t * omega_current;

omega_next = omega_current + delta_t *(-D*omega_current - sin(phi_current) + A*cos(psi_current));

psi_next =  psi_current + delta_t*omega_N;

end