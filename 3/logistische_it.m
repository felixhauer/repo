clc
clear

r = 3.5;
N = 1000;
X = 1:N;
Y = zeros(1,N);

x_start = 0.1; 

x_current = x_start; 
Y(1) = x_start; 

for i = 1:N

    x_next = logistic(x_current,r);
    x_current = x_next;

    if i>1
        Y(i) = x_current;
    end

end

plot(X,Y, '.')


function res = logistic(x,r)
res = r*x*(1-x);
end