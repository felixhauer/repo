clc 
clear

N = 100;
K = 5;
T_current = 5;
T_delta = 0.25;



for k = 2:N

   T_next = U(k,K,T_current, T_delta)
   T_current = T_next;
   
   
end


function T_next = U(k,K, T_last, T_delta)
 
if mod(k,K) == 0
    T_next = T_last - T_delta;
else
    T_next = T_last;
end

end


function p = P(c_current, c_next, T_current)

if c_next <= c_current
    p = 1;
else
    p = exp(-((c_next-c_current)/T_current));
end

end