clc
clear

N = 100;
K = 5;
T_current = 5;
T_delta = 0.25;

marker_size = 30;
Dimension = 15;
J_0 = -1; 

point = [2 2];

A_current = get_random_modell(Dimension);
E_current = H(A_current,J_0);

E = [E_current];


for k = 1:N

    A_next = get_neighborhood(A_current,point);  
    E_next = H(A_next,J_0);

  
    random_number = rand(1,1);
    prob =  P(E_current, E_next, T_current);

    if random_number < prob
        A_current = A_next;
        E_current = E_next;
        point = get_random_point(point,Dimension);
    end
        
   
    
    T_next = U(k,K,T_current, T_delta);
    T_current = T_next
    
    E = [E E_current];
    
    
    [X_red, Y_red, X_green, Y_green] = get_plot_data(A_current);
    hold on

    plot(X_red,Y_red, 'red.','MarkerSize',marker_size)
    plot(X_green,Y_green, 'green.','MarkerSize',marker_size)

    grid on
    linkdata on
    hold off


end

%plot([1:N+1], E)

E_current



function E = H(A,J_0)
E = 0;

Dimension = size(A);
Dimension = Dimension(1);

for Irow = 1:Dimension
for Icolumn = 1:Dimension        
    for Jrow = 1:Dimension
    for Jcolumn = 1:Dimension
            
        column_delta = abs(Icolumn - Jcolumn);
        row_delta = abs(Irow - Icolumn);
        
        if column_delta <= 1 && row_delta <= 1
            J = J_0;
        else
            J = 0;
        end
        
        
        
        add = J * A(Irow,Icolumn) * A(Jrow,Jcolumn);
        %add = J;
        
        E = E + add;
            
            
    end
    end
end
end
        
        


E = -1/2 * E;

end

function A = get_random_modell(dimension)
A = rand(dimension,dimension);
A(A<=0.5)=-1;
A(A>0.5)=1;
end

function [X_red, Y_red, X_green, Y_green] = get_plot_data(A)

N = size(A);
N = N(1);

X_red = [];
Y_red = [];

X_green = [];
Y_green = [];


for row = 1:N
    for column = 1:N
        
        
        if A(row, column) == 1
            X_green = [X_green, column];
            Y_green = [Y_green, row]; 
        else
            X_red = [X_red, column];
            Y_red = [Y_red, row]; 
        end
        
        
        
    end
end

end

function res = get_neighborhood(A,point)

A(point(2),point(1)) = A(point(2),point(1)) * -1;

res = A; 




end

function res = get_random_point(point,dim)

x = get_random_coordinate(point(1),dim);
y = get_random_coordinate(point(2),dim);

res = [x,y]; 

end

function res = get_random_coordinate(coordinate,dim)

if coordinate == 1
    delta = randi([0 1],1,1);
elseif coordinate == dim
    delta = randi([-1 0],1,1);
else
    delta = randi([-1 1],1,1);
end

res = coordinate + delta;

end

function T_next = U(k,K, T_last, T_delta)
 
if mod(k,K) == 0
    T_next = T_last - T_delta;
else
    T_next = T_last;
end

end

function p = P(c_current, c_next, T_current)

if c_next <= c_current
    p = 1;
else
    p = exp(-((c_next-c_current)/T_current));
end

end


