function X = box_mueller(N)
R1 = rand(N,1);
R2 = rand(N,1);

X = sqrt(-2*log(R1)) .* cos(2*pi.*R2);
end