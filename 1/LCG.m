function [ random_numbers ] = LCG( N, seed, a, c, m )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

random_numbers = zeros(N,1); 
random_numbers(1) = seed; 

for i = 1:(N-1)
    
    random_numbers(i+1) = mod( a * random_numbers(i) + c, m ); 
    
end

end

