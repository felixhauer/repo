function [error] = plot_monte_carlo(f,a,b,c,d,seed,i_max, f_name)

I_matlab = integral(f, a, b);

error = zeros(i_max,1);

for i = 1:i_max
    
    N = 10^i;
    
    I_monte_carlo = monte_carlo_integral(f,N,a,b,c,d,seed);
    
    error(i) = abs(I_matlab - I_monte_carlo);
    
end

i = (1:i_max);


plot(i,error, '-o')
xlabel('log_{10}(N)')
ylabel('absolute error')
title(f_name);
print(f_name,'-dpng')

end

