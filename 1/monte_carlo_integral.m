function I = monte_carlo_integral(f, N, a, b, c, d, seed)

if nargin == 7
    rand('seed', seed);
end

random_numbers = rand(N*2,1);

x = random_numbers(1:N) * (b-a) + a;
y = random_numbers((N+1):end) * (d-c) + c;

number_point_under_graph = sum(y<=f(x));

V = (b-a)*(d-c);

I = (number_point_under_graph * V) / N; 

end