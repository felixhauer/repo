clc
clear

f = @(x) exp(-1/2.*x.^2);
f_name = 'normal distribution';

a = -20;
b = 20;
c = 0;
d = 1;
i_max = 7; 

I_matlab = integral(f, a, b);

seed = 1;

error = zeros(i_max,1);


for i = 1:i_max
    
    N = 10^i;
    
    I_monte_carlo = monte_carlo_integral(f,N,a,b,c,d,seed);
    
    error(i) = abs(I_matlab - I_monte_carlo);
    
end

i=(1:i_max)'
error

plot(i,error, '-o')
xlabel('log_{10}(N)')
ylabel('absolute error')
title(f_name);
print('BarPlot','-dpng')





