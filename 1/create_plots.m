clc
clear

f = @(x) exp(-1/2.*x.^2);
f_name = 'normal distribution';

a = -20;
b = 20;
c = 0;
d = 1;
i_max = 7;
seed = 1; 

errors = plot_monte_carlo(f,a,b,c,d,seed,i_max, f_name)

clear

f = @(x) sin(x) + 1;
f_name = 'sin x + 1';

a = 0;
b = pi;
c = 0;
d = 2;
i_max = 7;
seed = 5; 

errors = plot_monte_carlo(f,a,b,c,d,seed,i_max, f_name)


clear

f = @(x) 2.*x.^2 + 3.*x + 1;
f_name = 'polynom';

a = 0;
b = 10;
c = 0;
d = f(10) + 1;
i_max = 7;
seed = 1; 

errors = plot_monte_carlo(f,a,b,c,d,seed,i_max, f_name)