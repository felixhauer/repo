clc
clear

N = 10000000;

random_x = rand(N,1);
random_y = rand(N,1);

number_inside_circle = 0;

for i =1:N
    quadratic_sum = random_x(i)^2 + random_y(i)^2;
    if quadratic_sum <= 1
        number_inside_circle = number_inside_circle + 1;
    end
end

numeric_pi = number_inside_circle / N * 4
        